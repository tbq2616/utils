package utils

import (
	"encoding/json"

	"github.com/satori/go.uuid"
)

func GetUuid() string {
	// error e
	u1 := uuid.Must(uuid.NewV4(), nil)
	return u1.String()
}

func GetJsonString(v *interface{}) (string, error) {
	rt, er := json.Marshal(v)
	if er != nil {
		return "", er
	}
	return string(rt), nil
}

// //一行代码 把array/slice转成逗号分隔的字符串
// strings.Replace(strings.Trim(fmt.Sprint(array_or_slice), "[]"), " ", ",", -1)

// //最佳方式：根据map的长度，新建一个数组，遍历map逐个压入
// func getKeys1(m map[int]int) []int {
// 	// 数组默认长度为map长度,后面append时,不需要重新申请内存和拷贝,效率较高
// 	j := 0
// 	keys := make([]int, len(m))
// 	for k := range m {
// 		keys[j] = k
// 		j++
// 	}
// 	return keys
// }
