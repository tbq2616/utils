package utils

import (
	"log"

	"github.com/Unknwon/goconfig"
)

type Cfg struct {
	c *goconfig.ConfigFile
}

func (cfg *Cfg) SetFile(file string) *Cfg {
	var err error
	cfg.c, err = goconfig.LoadConfigFile(file) ///home/tboqi/code/go/src/tbq/mqreceiver/
	if err != nil {
		log.Fatal(err)
	}
	return cfg
}

//读取单个值
func (cfg *Cfg) GetValue(section, key string) string {
	value, err := cfg.c.GetValue(section, key)
	if err != nil {
		log.Fatal(err)
	}
	return value
}

func NewCfg(file string) *Cfg {
	cfg := new(Cfg)
	cfg.SetFile(file)
	return cfg
}

/*
func main() {


	value, err1 := cfg.GetValue("mysql", "username")
	log.Print(value, err1)
	// valut, err2 := cfg.Int("must", int)
	//读取整个区
	sec, err3 := cfg.GetSection("mysql")
	log.Print(sec, err3)
}
*/
