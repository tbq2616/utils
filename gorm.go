package utils

import (
	// "gitee.com/tbq_go_packages/utils"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type GormHelp struct {
	db *gorm.DB
}

func (gh *GormHelp) conn() {
	var err error
	gh.db, err = gorm.Open("mysql", "work:111111@/test?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic("failed to connect database")
	}
}

func (gh *GormHelp) close() {
	gh.db.Close()
}

func Dbconn(cfg *Cfg) *gorm.DB {
	db, err := gorm.Open("mysql", cfg.GetValue("mysql", "dbuser")+":"+cfg.GetValue("mysql", "dbpass")+"@/"+cfg.GetValue("mysql", "dbname")+"?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		panic("failed to connect database")
	}
	return db
}
