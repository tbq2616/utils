package utils

import (
	"github.com/savsgio/atreugo/v11"
)

func AtreugoGetServer(addr string) *atreugo.Atreugo {
	config := atreugo.Config{
		Addr: addr,
	}
	server := atreugo.New(config)
	return server
}
